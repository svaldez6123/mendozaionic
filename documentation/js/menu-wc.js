'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">codelab-angular-devops documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-37bedd50ad2139b9f3761b31f34e0c7c"' : 'data-target="#xs-components-links-module-AppModule-37bedd50ad2139b9f3761b31f34e0c7c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-37bedd50ad2139b9f3761b31f34e0c7c"' :
                                            'id="xs-components-links-module-AppModule-37bedd50ad2139b9f3761b31f34e0c7c"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ContactoPageModule.html" data-type="entity-link">ContactoPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ContactoPageModule-6226a1a36446c3f0280d812a318d1841"' : 'data-target="#xs-components-links-module-ContactoPageModule-6226a1a36446c3f0280d812a318d1841"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ContactoPageModule-6226a1a36446c3f0280d812a318d1841"' :
                                            'id="xs-components-links-module-ContactoPageModule-6226a1a36446c3f0280d812a318d1841"' }>
                                            <li class="link">
                                                <a href="components/ContactoPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ContactoPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/EdicionProductoPageModule.html" data-type="entity-link">EdicionProductoPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-EdicionProductoPageModule-453bf55b709555d4b9102ceebfaec161"' : 'data-target="#xs-components-links-module-EdicionProductoPageModule-453bf55b709555d4b9102ceebfaec161"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-EdicionProductoPageModule-453bf55b709555d4b9102ceebfaec161"' :
                                            'id="xs-components-links-module-EdicionProductoPageModule-453bf55b709555d4b9102ceebfaec161"' }>
                                            <li class="link">
                                                <a href="components/EdicionProductoPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EdicionProductoPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/EdicionProductoProveedorPageModule.html" data-type="entity-link">EdicionProductoProveedorPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-EdicionProductoProveedorPageModule-0b4c77184f8f6f28cc7a540e12d765e9"' : 'data-target="#xs-components-links-module-EdicionProductoProveedorPageModule-0b4c77184f8f6f28cc7a540e12d765e9"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-EdicionProductoProveedorPageModule-0b4c77184f8f6f28cc7a540e12d765e9"' :
                                            'id="xs-components-links-module-EdicionProductoProveedorPageModule-0b4c77184f8f6f28cc7a540e12d765e9"' }>
                                            <li class="link">
                                                <a href="components/EdicionProductoProveedorPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EdicionProductoProveedorPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/EdicionProveedorPageModule.html" data-type="entity-link">EdicionProveedorPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-EdicionProveedorPageModule-3b2bc2c34f1796543c22d52bc044a2c5"' : 'data-target="#xs-components-links-module-EdicionProveedorPageModule-3b2bc2c34f1796543c22d52bc044a2c5"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-EdicionProveedorPageModule-3b2bc2c34f1796543c22d52bc044a2c5"' :
                                            'id="xs-components-links-module-EdicionProveedorPageModule-3b2bc2c34f1796543c22d52bc044a2c5"' }>
                                            <li class="link">
                                                <a href="components/EdicionProveedorPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EdicionProveedorPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ProductoProveedorPageModule.html" data-type="entity-link">ProductoProveedorPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ProductoProveedorPageModule-a310deb5621f8f8d170e0b1651d1d584"' : 'data-target="#xs-components-links-module-ProductoProveedorPageModule-a310deb5621f8f8d170e0b1651d1d584"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ProductoProveedorPageModule-a310deb5621f8f8d170e0b1651d1d584"' :
                                            'id="xs-components-links-module-ProductoProveedorPageModule-a310deb5621f8f8d170e0b1651d1d584"' }>
                                            <li class="link">
                                                <a href="components/ProductoProveedorPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ProductoProveedorPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/Tab1PageModule.html" data-type="entity-link">Tab1PageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-Tab1PageModule-850f5d8cdc7ceeea7981735aadd79805"' : 'data-target="#xs-components-links-module-Tab1PageModule-850f5d8cdc7ceeea7981735aadd79805"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-Tab1PageModule-850f5d8cdc7ceeea7981735aadd79805"' :
                                            'id="xs-components-links-module-Tab1PageModule-850f5d8cdc7ceeea7981735aadd79805"' }>
                                            <li class="link">
                                                <a href="components/Tab1Page.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">Tab1Page</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/Tab2PageModule.html" data-type="entity-link">Tab2PageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-Tab2PageModule-a467cdf4e90abe92e30cac6845035cc9"' : 'data-target="#xs-components-links-module-Tab2PageModule-a467cdf4e90abe92e30cac6845035cc9"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-Tab2PageModule-a467cdf4e90abe92e30cac6845035cc9"' :
                                            'id="xs-components-links-module-Tab2PageModule-a467cdf4e90abe92e30cac6845035cc9"' }>
                                            <li class="link">
                                                <a href="components/Tab2Page.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">Tab2Page</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/Tab3PageModule.html" data-type="entity-link">Tab3PageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-Tab3PageModule-a4d8a6e7679fa53e0b113808156cfca7"' : 'data-target="#xs-components-links-module-Tab3PageModule-a4d8a6e7679fa53e0b113808156cfca7"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-Tab3PageModule-a4d8a6e7679fa53e0b113808156cfca7"' :
                                            'id="xs-components-links-module-Tab3PageModule-a4d8a6e7679fa53e0b113808156cfca7"' }>
                                            <li class="link">
                                                <a href="components/Tab3Page.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">Tab3Page</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabsPageModule.html" data-type="entity-link">TabsPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TabsPageModule-2e6139b3a3c561b39ce76f6d6856c021"' : 'data-target="#xs-components-links-module-TabsPageModule-2e6139b3a3c561b39ce76f6d6856c021"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabsPageModule-2e6139b3a3c561b39ce76f6d6856c021"' :
                                            'id="xs-components-links-module-TabsPageModule-2e6139b3a3c561b39ce76f6d6856c021"' }>
                                            <li class="link">
                                                <a href="components/TabsPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TabsPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabsPageRoutingModule.html" data-type="entity-link">TabsPageRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});