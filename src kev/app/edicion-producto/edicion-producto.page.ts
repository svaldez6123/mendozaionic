import { Component, OnInit } from '@angular/core';
import {NavController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {Md5} from 'ts-md5/dist/md5';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-edicion-producto',
  templateUrl: './edicion-producto.page.html',
  styleUrls: ['./edicion-producto.page.scss'],
})
export class EdicionProductoPage implements OnInit {

  public producto:any;
  constructor(public navCtrl: NavController, private storage:Storage,
             public route:ActivatedRoute) {

      this.route.queryParams.subscribe(params =>{
        if(params["producto"]){
          this.producto=JSON.parse(params["producto"]);
        }else{
          this.producto={};
        }
      })

   }
  

  ngOnInit() {
  }
  cancelar(){
  this.navCtrl.navigateBack('');
  }
  guardar(){
    this.storage.get("productos").then((productos)=>{
      if(this.producto.id){
        let productoBuscado=this.producto;
        let productoEncontrado=productos.
        filter(function(e,index){return e.id == productoBuscado.id;})[0];
        productos[productos.indexOf(productoEncontrado)]=productoBuscado;
        this.storage.set("productos", productos);
      }
      else{
        this.producto.id=Md5.hashStr(new Date().getTime().toString());
      if(productos){
        productos.push(this.producto);this.storage.set("productos",productos);
      }
      else{
        let arrayProductos=[];
        arrayProductos.push(this.producto);
        this.storage.set("productos", arrayProductos);
      }
    }
    this.navCtrl.navigateBack('');
  });
}
}
