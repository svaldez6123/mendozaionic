import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdicionProveedorPage } from './edicion-proveedor.page';

describe('EdicionProveedorPage', () => {
  let component: EdicionProveedorPage;
  let fixture: ComponentFixture<EdicionProveedorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdicionProveedorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdicionProveedorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
