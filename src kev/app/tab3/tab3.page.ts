import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {NavigationExtras} from '@angular/router';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  public proveedores:[];

constructor(public navCtrl: NavController, private storage:Storage)
{
this.storage.get("proveedores").then((proveedoresDeMemoria) => {
  this.proveedores=proveedoresDeMemoria;
})
}
ionViewDidEnter(){
  this.storage.get("proveedores").then((proveedoresDeMemoria) => {
    this.proveedores=proveedoresDeMemoria;
  })
}
crearProveedor(){
  this.navCtrl.navigateForward('edicion-proveedor');
}
modificarProveedor(proveedor){
  let navigationExtras:NavigationExtras={
    queryParams:{
      proveedor:JSON.stringify(proveedor)
    }
  }
  this.navCtrl.navigateForward(['edicion-proveedor'],navigationExtras);
}
}
