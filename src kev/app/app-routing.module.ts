import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'contacto', loadChildren: './contacto/contacto.module#ContactoPageModule' },
  { path: 'edicion-producto', loadChildren: './edicion-producto/edicion-producto.module#EdicionProductoPageModule' },
  { path: 'edicion-proveedor', loadChildren: './edicion-proveedor/edicion-proveedor.module#EdicionProveedorPageModule' },
  { path: 'edicion-producto-proveedor', loadChildren: './edicion-producto-proveedor/edicion-producto-proveedor.module#EdicionProductoProveedorPageModule' },
  { path: 'producto-proveedor', loadChildren: './producto-proveedor/producto-proveedor.module#ProductoProveedorPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
