import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {NavigationExtras} from '@angular/router';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {   

  public productos:[];

constructor(public navCtrl: NavController, private storage:Storage)
{
this.storage.get("productos").then((productosDeMemoria) => {
  this.productos=productosDeMemoria;
})
}

ionViewDidEnter(){
  this.storage.get("productos").then((productosDeMemoria) => {
    this.productos=productosDeMemoria;
  })
}

crearProducto(){
  this.navCtrl.navigateForward('edicion-producto');
}

modificarProducto(producto){
  let navigationExtras:NavigationExtras={
    queryParams:{
      producto:JSON.stringify(producto)
    }
  }
  this.navCtrl.navigateForward(['edicion-producto'],navigationExtras);
}

 }