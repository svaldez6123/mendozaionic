import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EdicionProveedorPage } from './edicion-proveedor.page';

const routes: Routes = [
  {
    path: '',
    component: EdicionProveedorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EdicionProveedorPage]
})
export class EdicionProveedorPageModule {}
