import { Component, OnInit } from '@angular/core';
import {NavController, AlertController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {Md5} from 'ts-md5/dist/md5';
import {ActivatedRoute} from "@angular/router";
@Component({
  selector: 'app-edicion-proveedor',
  templateUrl: './edicion-proveedor.page.html',
  styleUrls: ['./edicion-proveedor.page.scss'],
})
export class EdicionProveedorPage implements OnInit {
  public proveedor:any;
  constructor(public navCtrl: NavController, private storage:Storage,
    public route:ActivatedRoute, public alertCtrl:AlertController)
     {
      this.route.queryParams.subscribe(params => {
        if (params["proveedor"]) {
          this.proveedor = JSON.parse(params["proveedor"]);
        } else {
          this.proveedor = {};
        }
      });
    }

  ngOnInit() {
  }
cancelar(){
this.navCtrl.navigateBack('tabs/tab3');
}
guardar() {
  this.storage.get("proveedores").then((proveedores)=>{
    if (this.proveedor.id) {
      let proveedorBuscado=this.proveedor;
      let proveedorEncontrado= proveedores.filter(function(e,index){return e.id ==proveedorBuscado.id;})[0];
      proveedores[proveedores.indexOf(proveedorEncontrado)]=proveedorBuscado;
      this.storage.set("proveedores", proveedores);
    } else {
      this.proveedor.id = Md5.hashStr(new Date().getTime().toString());
      if( proveedores ) {
        proveedores.push(this.proveedor);
        this.storage.set("proveedores" , proveedores);
      } else{
        let arrayProveedores =[];
        arrayProveedores.push(this.proveedor);
        this.storage.set("proveedores", arrayProveedores);
      }
    }
    this.navCtrl.navigateBack("/tabs/tab3");
  });

}
}
