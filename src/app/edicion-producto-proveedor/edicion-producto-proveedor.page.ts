import { Component, OnInit } from '@angular/core';
import {Storage} from '@ionic/storage';
import {ActivatedRoute} from "@angular/router";
import {Md5} from 'ts-md5/dist/md5';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-edicion-producto-proveedor',
  templateUrl: './edicion-producto-proveedor.page.html',
  styleUrls: ['./edicion-producto-proveedor.page.scss'],
})
export class EdicionProductoProveedorPage implements OnInit {

  public proveedores:any;
  public producto:any;
  public producto_proveedor:any;

  constructor(private storage:Storage,public route:ActivatedRoute,
    private navCtrl:NavController) { 
      this.producto_proveedor={};
    this.route.queryParams.subscribe(params =>{
      this.producto=JSON.parse(params["producto"]);
      this.producto_proveedor.id_producto=this.producto.id;
    });

    this.storage.get("proveedores").then((proveedores)=>{
      this.proveedores=proveedores;
    });

  }

  ngOnInit() {
  }
  cancelar(){
    this.navCtrl.navigateBack('tabs/tab1');
    }
  guardar(){
    this.storage.get("producto_proveedores").then((producto_proveedores)=> {
      if(this.producto_proveedor.id){
        let productoProveedorBuscado=this.producto_proveedor;
        let productoProveedorEncontrado=producto_proveedores.filter(function(e,index){return e.id == productoProveedorBuscado.id;})[0];
        producto_proveedores[producto_proveedores.indexOf(productoProveedorEncontrado)]=productoProveedorBuscado;
        this.storage.set("producto_proveedores",producto_proveedores);
      }else{
        this.producto_proveedor.id=Md5.hashStr(new Date().getTime().toString());
        if(producto_proveedores){
          producto_proveedores.push(this.producto_proveedor);
          this.storage.set("producto_proveedores",producto_proveedores);
        }else{
          let arrayProductoProveedores=[];
          arrayProductoProveedores.push(this.producto_proveedor);
          this.storage.set("producto_proveedores", arrayProductoProveedores);
        } 
      }
      this.navCtrl.navigateBack('producto-proveedor');
    });
  }

}
