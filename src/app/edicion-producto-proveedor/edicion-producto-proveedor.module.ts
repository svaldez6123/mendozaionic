import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EdicionProductoProveedorPage } from './edicion-producto-proveedor.page';

const routes: Routes = [
  {
    path: '',
    component: EdicionProductoProveedorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EdicionProductoProveedorPage]
})
export class EdicionProductoProveedorPageModule {}
