import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import {Storage} from '@ionic/storage';
import { Router } from '@angular/router';
import { NavigationExtras } from "@angular/router";
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  public proveedores:any;

constructor(public navCtrl: NavController, 
            private storage:Storage, 
            private router: Router,
            public alertController : AlertController)
{
  this.proveedores=[];
  this.storage.get("proveedores").then((proveedorDeMeroria) => {
    this.proveedores = proveedorDeMeroria;
  });
}
  ionViewDidEnter() {
    this.storage.get("proveedores").then(proveedorDeMeroria => {
      this.proveedores = proveedorDeMeroria;
    })
  }
modificarProveedor(proveedor) {
  let navigationExtras:NavigationExtras={
    queryParams:{
      proveedor:JSON.stringify(proveedor)
    }
  }
  this.navCtrl.navigateForward(['edicion-proveedor'], navigationExtras);
}

eliminarProveedores(proveedor){ 
  /*let productoEncontrado=this.productos.filter(function(e,index){return e.id == producto.id;})[0];*/  
  this.proveedores.splice((this.proveedores.indexOf(proveedor)),1);
  this.storage.set("proveedores",this.proveedores);
}
crearProveedor(){
  this.navCtrl.navigateForward('edicion-proveedor');
}
/*Para que salga la alerta de eliminar */
async presentAlerta(){
  const alert = await this.alertController.create({
    header: 'Alerta!',
    message: 'Esta seguro que desea <strong>eliminar</strong>!',
    buttons: [{
      text:'Cancelar',
      role:'cancel',
      handler:()=>{
        console.log('You clicked me ');
      }
    },
      {text:'Aceptar',
      cssClass:'secondary',
      handler:()=>{
        console.log('eliminado');
      this.eliminarProveedores('proveedor');
      }
    }
    ]
  })
  await alert.present();
}
}
