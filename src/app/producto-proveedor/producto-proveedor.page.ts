import { Component, OnInit } from '@angular/core';

import {Storage} from '@ionic/storage';
import {Md5} from 'ts-md5/dist/md5';
import {ActivatedRoute} from "@angular/router";
import {NavigationExtras} from '@angular/router';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-producto-proveedor',
  templateUrl: './producto-proveedor.page.html',
  styleUrls: ['./producto-proveedor.page.scss'],
})
export class ProductoProveedorPage {
  public producto:any;
  public producto_proveedores:any;
  public proveedores:any;
  
  constructor(private storage:Storage,
    public route:ActivatedRoute,public navCtrl: NavController) { 
    this.route.queryParams.subscribe(params =>{
      if(params["producto"]){
        this.producto=JSON.parse(params["producto"]);
      }else{
        this.producto={};
      }
    });
  }

  ionViewDidEnter(){
    //Recuperar los precios de los proveedores del producto
    let producto_a_buscar=this.producto;
    this.producto_proveedores=[];
    this.storage.get("producto_proveedores").then((producto_proveedores)=>{
      if(producto_proveedores){
        this.producto_proveedores=producto_proveedores.filter(function(e,index){return e.id_producto ==producto_a_buscar.id ;});
      }else{
        this.producto_proveedores=[];
      }
      
    });

    //Recupera la tabla proveedores
    this.proveedores=[];
    this.storage.get("proveedores").then((proveedores)=> {
      this.proveedores=proveedores;
    });
  }
  
  eliminarProducto_Proveedores(producto_proveedor){ 
    /*let producto_proveedores=this.productos.filter(function(e,index){return e.id == producto_proveedores.id;})[0];*/ 
    this.producto_proveedores.splice((this.producto_proveedores.indexOf(producto_proveedor)),1);
    this.storage.set("producto_proveedores",this.producto_proveedores);
  }
  
  crearPrecio(){
    let navigationExtras:NavigationExtras={
      queryParams:{
        producto:JSON.stringify(this.producto)
      }
    }
    this.navCtrl.navigateForward(['edicion-producto-proveedor'],navigationExtras);
  }

  mostrarNombreProveedor(id_proveedor){
    let proveedorEncontrado=this.proveedores.filter(function(e,index){return e.id == id_proveedor;})[0];
    if(proveedorEncontrado){
      return proveedorEncontrado.nombre; 
    }else{
      return "";
    }
  }

  

}
