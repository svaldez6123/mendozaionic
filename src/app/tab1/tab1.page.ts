import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {NavigationExtras} from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  public productos:any;

constructor(public navCtrl: NavController, 
            private storage:Storage,
            public alertController : AlertController)
{
  this.productos=[];
}
/*Para actualizar los cambios, no sirve bien*/
ionViewDidEnter(){
  this.storage.get("productos").then((productosDeMemoria) => {
    this.productos=productosDeMemoria;
  })
}

crearProducto(){
  this.navCtrl.navigateForward('edicion-producto');
}

modificarProducto(producto){
  let navigationExtras:NavigationExtras={
    queryParams:{
      producto:JSON.stringify(producto)
    }
  }
  this.navCtrl.navigateForward(['edicion-producto'],navigationExtras);
}
eliminarProducto(producto){ 
  /*let productoEncontrado=this.productos.filter(function(e,index){return e.id == producto.id;})[0];*/
  this.productos.splice((this.productos.indexOf(producto)),1);
  this.storage.set("productos",this.productos);  
}
verPrecios(producto){
  let navigationExtras:NavigationExtras={
    queryParams:{
      producto:JSON.stringify(producto)
    }
  }
  this.navCtrl.navigateForward(['producto-proveedor'],navigationExtras);
}
/*Para que salga la alerta de eliminar */
async presentAlert(){
  const alert = await this.alertController.create({
    header: 'Alerta!',
    message: 'Esta seguro que desea <strong>eliminar</strong>!',
    buttons: [{
      text:'Cancelar',
      role:'cancel',
      handler:()=>{
        console.log('You clicked me');
      }
    },
      {text:'Aceptar',
      cssClass:'secondary',
      handler:()=>{
        console.log('eliminado');
      this.eliminarProducto('producto');
      }
    }]
  })
  await alert.present();
}
}