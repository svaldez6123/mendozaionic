import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdicionProductoPage } from './edicion-producto.page';

describe('EdicionProductoPage', () => {
  let component: EdicionProductoPage;
  let fixture: ComponentFixture<EdicionProductoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdicionProductoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdicionProductoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
